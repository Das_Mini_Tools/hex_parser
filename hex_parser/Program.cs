﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace hex_parser
{
    internal class Program
    {
        private static readonly byte[] hexBytes = { 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46 };

        private static void FileParser(FileInfo fi)
        {
            using (var @in = File.OpenRead(fi.FullName))
            using (var @out = File.Create(fi.FullName + ".parsed", 4096, FileOptions.SequentialScan))
            {
                byte[] buffer = new byte[4096];

                int readBytes;

                while ((readBytes = @in.Read(buffer, 0, buffer.Length)) > 0)
                    for (int i = 0; i < readBytes; i++)
                        if (hexBytes.Contains(buffer[i]))
                            @out.WriteByte(buffer[i]);
            }
        }

        private static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("hex_parser v" + Assembly.GetExecutingAssembly().GetName().Version + " by Dasanko, written for catalinnc." + Environment.NewLine);

                Console.WriteLine("ERROR: Missing target file." + Environment.NewLine);

                Console.WriteLine("Usage: hex_parser <file>");

                return;
            }

            var fi = new FileInfo(args[0]);

            if (fi.Length <= 150 * 1024 * 1024)
                MemoryParser(fi);
            else
                FileParser(fi);

            Console.WriteLine("Finished!");
        }

        private static void MemoryParser(FileInfo fi)
        {
            byte[] contents = File.ReadAllBytes(fi.FullName);

            using (var ms = new MemoryStream(contents))
            using (var fs = File.Create(fi.FullName + ".parsed", 4096, FileOptions.SequentialScan))
                while (ms.Position < ms.Length)
                {
                    byte @byte = (byte)ms.ReadByte();

                    if (hexBytes.Contains(@byte))
                        fs.WriteByte(@byte);
                }
        }
    }
}
